package com.ltmonitor.service;

import com.ltmonitor.entity.TerminalCommand;

public interface ITerminalCommandService {

	public  void SaveTerminalCommand(TerminalCommand tc);
	
	TerminalCommand getCommandBySn(String sn);

}